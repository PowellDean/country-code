unit module Country::Code;

sub VERSION() is export { v0.0.5; }

enum Codes is export <ISO3166_2 ISO3166_3 ISO3166_NUM>;

my %codes2;
my %codes3;
my %codesNum;
my %countries;

my @data = $=finish.lines;

for @data -> $line {
    my @fields = split /\,/, $line;

	%codes2{"@fields[0]"} = @fields[1], @fields[2], @fields[3];
	%codes3{"@fields[1]"} = @fields[0], @fields[2], @fields[3];
	%codesNum{@fields[2]} = @fields[0], @fields[1], @fields[3];
	
	%countries{"@fields[3]".uc} = @fields[0], @fields[1],
		@fields[2], @fields[3];
}

say codeToCountry(124, ISO3166_NUM);
say codeToCountry("bv", ISO3166_2);
say countryToCode("Burkina Faso", ISO3166_3);
say codeToCode(652, ISO3166_3);
printSupportedCodes();

sub codeToCode($code!, $codesetOut? = ISO3166_2) is export {
	my $check;
	my $codesetIn;
	
	#OK, first we determine what typpe of code has been passed IN:
	#	a number, or a 2- or 3-character code
	#   if none of these, just return (Any)
	given ($code.WHAT) {
		when Int {
			$check = sprintf("%03d", $code);
			$codesetIn = ISO3166_NUM;
		}
		when Str {
			$check = $code;
			given ($code.chars) {
				when 2 { $codesetIn = ISO3166_2; }
				when 3 { $codesetIn = ISO3166_3; }
				default { return Any; }
			}
		}
		default {
			return Any;
		}
	}
	
	# We've determined what code we sent in, now let's return the
	#requested code type
	
	given ($codesetIn) {
		when ISO3166_2 {
			given ($codesetOut) {
				when ISO3166_2 { return $check if %codes2{"$check"}; }
				when ISO3166_3 {
					return %codes2{"$check"}[0];
				}
				when ISO3166_NUM {
					return %codes2{"$check"}[1];
				}
			}
		}
		when ISO3166_3 {
			given ($codesetOut) {
				when ISO3166_2 {
					return %codes3{"$check"}[0];
				}
				when ISO3166_3 {
					return $code if %codes3{"$check"};
				}
				when ISO3166_NUM {
					return %codes3{"$check"}[2];
				}
			}
		}
		when ISO3166_NUM {
			given ($codesetOut) {
				when ISO3166_2 {
					return %codesNum{$check}[0];
				}
				when ISO3166_3 {
					return %codesNum{$check}[1] if %codesNum{$check};
				}
				when ISO3166_NUM { return $check; }
			}
		}
	}
	
	return Any;
}

sub codeToCountry($code!, $codeset? = ISO3166_2) is export {
	my $check = $code.uc;
	if $codeset == ISO3166_NUM {
		return Any if $code.WHAT !~~ Int;
		$check = sprintf("%03d", $check);
		
		return %codesNum{$check}[2];
	}
	
	return %codes3{"$check"}[2] if $codeset == ISO3166_3;
	return %codes2{"$check"}[2] if $codeset == ISO3166_2;
	
	return Any;
}

sub countryToCode($name!, $codeset? = ISO3166_2) is export {
	my $check = $name.uc;

	given ($codeset) {
		when ISO3166_NUM { 
			return %countries{$check}[2] if $check ~~ %countries;
		}
		when ISO3166_2 {
			return %countries{"$check"}[0] if $check ~~ %countries;
		}
		when ISO3166_3 {
			return %countries{"$check"}[1] if $check ~~ %countries;
		}
	}
	
	return Any;
}

sub printSupportedCodes() is export {

	say "The following is a list of currently supported codesets and the";
	say "value attached to them (codeset => value). When asked for a codeset";
	say "you may specify by either name or by value. Don't enclose the";
	say "codeset name in quotes:";
	for Codes.enums {
		say $_;
	}
}

=finish
AD,AND,020,Andorra
AE,ARE,784,United Arab Emirates
AF,AFG,004,Afghanistan
AG,ATG,028,Antigua and Barbuda
AI,AIA,660,Anguilla
AL,ALB,008,Albania
AM,ARM,051,Armenia
AO,AGO,024,Angola
AQ,ATA,010,Antarctica
AR,ARG,032,Argentina
AS,ASM,016,American Samoa
AT,AUT,040,Austria
AU,AUS,036,Australia
AW,ABW,533,Aruba
AX,ALA,248,Aland Islands
AZ,AZE,031,Azerbaijan
BA,BIH,070,Bosnia and Herzegovina
BB,BRB,052,Barbados
BD,BGD,050,Bangladesh
BE,BEL,056,Belgium
BF,BFA,854,Burkina Faso
BG,BGR,100,Bulgaria
BH,BHR,048,Bahrain
BI,BDI,108,Burundi
BJ,BEN,204,Benin
BL,BLM,652,Saint-Barthélemy
BM,BMU,060,Bermuda
BN,BRN,096,Brunei Darussalam
BO,BOL,068,Bolivia
BQ,BES,535,Bonaire
BR,BRA,076,Brazil
BS,BHS,044,Bahamas
BT,BTN,064,Bhutan
BV,BVT,074,Bouvet Island
BW,BWA,072,Botswana
BY,BLR,112,Belarus
BZ,BLZ,084,Belize
CA,CAN,124,Canada
CC,CCK,166,Cocos (Keeling) Islands
CD,COD,180,Democratic Republic of Congo
CF,CAF,140,Central African Republic
CG,COG,178,Congo
CH,CHE,756,Switzerland
CI,CIV,384,Côte d'Ivoire
CK,COK,184,Cook Islands
CL,CHL,152,Chile
CM,CMR,120,Cameroon
CN,CHN,156,China
CO,COL,170,Colombia
CR,CRI,188,Costa Rica
CU,CUB,192,Cuba
CV,CPV,132,Cape verde
CX,CXR,162,Christmas Island
CY,CYP,196,Cyprus
CZ,CZE,203,Czech Republic
DE,DEU,276,Germany
DJ,DJI,262,Djibouti
DK,DNK,208,Denmark
DM,DMA,212,Dominica
DO,DOM,214,Dominican Republic
DZ,DZA,012,Algeria
EC,ECU,218,Ecuador
EE,EST,233,Estonia
EG,EGY,818,Egypt
EH,ESH,732,Western Sahara
ER,ERI,232,Eritrea
ES,ESP,724,Spain
ET,ETH,231,Ethiopia
FI,FIN,246,Finland
FJ,FJI,242,Fiji
FM,FSM,583,Federated States of Micronesia
FO,FRO,234,Faroe Islands
FR,FRA,250,France